class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        return sum(product.get_price() for product in self.products)

    def get_total_quantity_of_products(self):
        return sum(product.quantity for product in self.products)

    def purchase(self):
        if not self.products:
            return "!!! Cannot set the orders without any selected products !!!"
        else:
            return f"Orders for the amount of {self.get_total_price()} has been set. Confirmation sent on email: {self.customer_email}."


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity
